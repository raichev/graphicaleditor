<?php

namespace GraphicEditor\Commands;

use GraphicEditor\Models\Canvas;
use Exception;

class Command
{
    const COLOR = 3;
    const FIRST_CHANGE_ROW = 2;
    const SECOND_CHANGE_ROW = 3;
    const FIRST_CHANGE_COL = 1;
    const SECOND_CHANGE_COL = 2;
    const Y_POSITION_HOR = 3;
    const HOR_VER_COLOR = 4;
    const M = 1; // col
    const N = 2; // row
    const MAX_VALUE_X_Y = 250;
    const MIN_VALUE_X_Y = 1;
    const DEFAULT_COLOR = '0';

    private $canvas;

    public function __construct(Canvas $canvas)
    {
        $this->canvas = $canvas;
    }

    /**
     * Init empty canvased based on x,y entered from console
     *
     * @param int $m
     * @param int $n
     * @throws Exception if coordinates are invalid
     */
    public function initEmptyCanvas(int $m, int $n) : void
    {
        if ($n > self::MAX_VALUE_X_Y || $m < self::MIN_VALUE_X_Y) {
            throw new Exception('Invalid coordinates, please enter coordinates between 1 and 250');
        }

        $this->canvas->setGrid([]);
        $grid = [];
        for ($i = 1; $i <= $n; $i++) {
            for ($j = 1; $j <= $m; $j++) {
                $grid[$i][$j]['x'] = $i;
                $grid[$i][$j]['y'] = $j;
                $grid[$i][$j]['color'] = self::DEFAULT_COLOR;
            }
        }

        $this->canvas->setGrid($grid);
    }

    /**
     * Change color of specific coordinates of canvas
     *
     * @param array $command
     * @throws Exception if coordinates are invalid
     */
    public function colorPixels(array $command) : void
    {
        if (count($command) != 4) {
            throw new Exception('Invalid entered command for change color pixes.');
        }

        $grid = $this->canvas->getGrid();
        if (!isset($grid[$command[self::N]][$command[self::M]])) {
            throw new Exception('Invalid coordinates.');
        }
        $grid[$command[self::N]][$command[self::M]]['color'] = strtoupper($command[self::COLOR]);

        $this->canvas->setGrid($grid);
    }

    /**
     * Change color of horizontal order of pixels
     *
     * @param array $command
     * @throws Exception if coordinates are invalid
     */
    public function horizontalPixelColorChange(array $command) : void
    {
        if (count($command) != 5) {
            throw new Exception('Invalid entered command for vertical pixel color changes');
        }

        $grid = $this->canvas->getGrid();
        if (!isset($grid[$command[self::FIRST_CHANGE_COL]][$command[self::Y_POSITION_HOR]])
            && !isset($grid[$command[self::SECOND_CHANGE_COL]][$command[self::Y_POSITION_HOR]])) {
            throw new Exception('Invalid coordinates.');
        }

        for ($i = $command[self::FIRST_CHANGE_COL]; $i <= $command[self::SECOND_CHANGE_COL]; $i++) {
            $grid[$command[self::Y_POSITION_HOR]][$i]['color'] = strtoupper($command[self::HOR_VER_COLOR]);
        }

        $this->canvas->setGrid($grid);
    }

    /**
     * Change color of vertical order of pixels
     *
     * @param array $command
     * @throws Exception if coordinates are invalid
     */
    public function verticalPixelColorChange(array $command) : void
    {
        if (count($command) != 5) {
            throw new Exception('Invalid entered command for vertical pixel color changes');
        }

        $grid = $this->canvas->getGrid();
        if (!isset($grid[$command[self::FIRST_CHANGE_ROW]][$command[self::M]])
            && !isset($grid[$command[self::SECOND_CHANGE_ROW]][$command[self::M]])) {
            throw new Exception('Invalid coordinates.');
        }

        for ($i = $command[self::FIRST_CHANGE_ROW]; $i <= $command[self::SECOND_CHANGE_ROW]; $i++) {
            $grid[$i][$command[self::M]]['color'] = strtoupper($command[self::HOR_VER_COLOR]);
        }

        $this->canvas->setGrid($grid);
    }

    /**
     * Change color of area based on coordinates entered from console
     *
     * @param array $command
     * @throws Exception if coordinates are invalid
     */
    public function fillRegionWithColor(array $command) : void
    {
        $colors = [];
        if (count($command) != 4) {
            throw new Exception('Invalid entered command for fill region with color');
        }

        $grid = $this->canvas->getGrid();
        if (!isset($grid[$command[self::N]][$command[self::M]])) {
            throw new Exception('Invalid coordinates.');
        }

        for ($i = 1; $i <= count($grid); $i++) {
            for ($j = 1; $j <= count($grid[$i]); $j++) {
                if ($i <= $command[self::N] && $j <= $command[self::M]) { // Pixel Area
                    $colors[] = $grid[$i][$j]['color'];
                    $grid[$i][$j]['color'] = strtoupper($command[self::COLOR]);
                } elseif ($i <= $command[self::N] + 1 && $j <= $command[self::M] + 1 && $i !== $j) { // Neighbours of Pixel Area
                    $grid[$i][$j]['color'] = strtoupper($command[self::COLOR]);
                } elseif (in_array($grid[$i][$j]['color'], $colors)) { // Check other pixels for equals with pixel area colors
                    $grid[$i][$j]['color'] = strtoupper($command[self::COLOR]);
                }
            }
        }

        $this->canvas->setGrid($grid);
    }
}
