<?php

namespace GraphicEditor\Views;

class ConsoleView
{
    /**
     * Render canvas with specific grid
     *
     * @param array $grid
     */
    public function renderCanvas(array $grid) : void
    {
        for ($i = 1; $i <= count($grid); $i++) {
            for ($j = 1; $j <= count($grid[$i]); $j++) {
               echo $grid[$i][$j]["color"];
            }
            echo "\n";
        }
    }
}
