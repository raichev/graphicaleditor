<?php

namespace GraphicEditor\Controllers;

use GraphicEditor\Views\ConsoleView;
use GraphicEditor\Models\Canvas;
use GraphicEditor\Commands\Command;
use Exception;

class GraphicEditorConsoleController
{
    const COMMAND = 0;
    const M = 1;
    const N = 2;
    private $canvas;
    private $m;
    private $n;

    public function __construct(Canvas $canvas)
    {
        $this->canvas = $canvas;
        $this->m = 0;
        $this->n = 0;
    }

    /**
     * Check which command is needed to execute
     *
     * @param string $command
     * @throws Exception when some of checks fails
     */
    public function executeCommand(string $command) : void
    {
        $commandInstance = new Command($this->canvas);
        $visualisation = new ConsoleView();

        $explodedCommand = explode(' ', $command);
        $command = strtoupper($explodedCommand[self::COMMAND]);
        if ($this->canvas->getGrid() === []  && $command !== 'I') {
            throw new Exception('First you need to init canvas');
        }

        switch ($command) {
            case 'I':
                $this->m = $explodedCommand[self::M];
                $this->n = $explodedCommand[self::N];
                $commandInstance->initEmptyCanvas($this->m, $this->n);
                break;
            case 'C':
                $commandInstance->initEmptyCanvas($this->m, $this->n);
                break;
            case 'L':
                $commandInstance->colorPixels($explodedCommand);
                break;
            case 'V':
                $commandInstance->verticalPixelColorChange($explodedCommand);
                break;
            case 'H':
                $commandInstance->horizontalPixelColorChange($explodedCommand);
                break;
            case 'F':
                $commandInstance->fillRegionWithColor($explodedCommand);
                break;
            case 'S':
                $visualisation->renderCanvas($this->canvas->getGrid());
                break;
            case 'X':
                $this->canvas->setGrid([]);
                break;
            default:
                throw new Exception('Invalid entered command');
        }
    }
}
