<?php

namespace GraphicEditor\Models;

class Canvas
{
    private $grid;

    public function __construct()
    {
        $this->grid = [];
    }

    /**
     * Get current grid
     *
     * @return array
     */
    public function getGrid() : array
    {
        return $this->grid;
    }

    /**
     * Set array to grid
     *
     * @param array $grid
     */
    public function setGrid(array $grid) : void
    {
        $this->grid = $grid;
    }
}
