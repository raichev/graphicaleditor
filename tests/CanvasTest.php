<?php

use PHPUnit\Framework\TestCase;
use GraphicEditor\Models\Canvas;

class CanvasTest extends TestCase
{
    public function testGetSetGrid()
    {
        $grid = new Canvas();
        $grid->setGrid(['test1', 'test2']);
        $expected = [0 => 'test1', 1 => 'test2'];
        $this->assertEquals($expected, $grid->getGrid());
    }
}
