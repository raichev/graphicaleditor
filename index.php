<?php
require  __DIR__ .'/vendor/autoload.php';

use GraphicEditor\Models\Canvas;
use GraphicEditor\Controllers\GraphicEditorConsoleController;

$canvas = new Canvas();
$graphicController = new GraphicEditorConsoleController($canvas);

do {
    try {
        echo "Enter command: ";
        $command = trim(fgets(STDIN));
        $graphicController->executeCommand($command);
    } catch (Exception $e) {
        echo $e->getMessage()."\n";
    }
} while ($canvas->getGrid() != []);
